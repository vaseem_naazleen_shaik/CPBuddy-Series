# Array-DS

An array is a type of data structure that stores elements of the same type in a contiguous block of memory. In an array, A, of size N, each memory location has some unique index, i(where 0 <= i <= N), that can be referenced as [i] (you may also see it written as ).

Given an array, A, of N integers, print each element in reverse order as a single line of space-separated integers.


## Input Format

The first line contains an integer, N(the number of integers in A).
The second line contains N space-separated integers describing A.

## Output Format

Print all N integers in A in reverse order as a single line of space-separated integers.
