# Unique Number 3

Given an array which contains all elements occurring k times, but one occurs only once. Find that unique element.

### Input Format

The first line consists of an integer T i.e number of test cases. The first line of each test case consists of two integers n and k. The next line consists of n spaced integers.

### Constraints

1<=T<=100
2<=k 1<=a[i]<=10000

### Output Format

Print the required output.

### Sample Input 0
<pre>
2
7 3
6 2 5 2 2 6 6
5 4
2 2 2 10 2
</pre>
### Sample Output 0
<pre>
5
10
</pre>
