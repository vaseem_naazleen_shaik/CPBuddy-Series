# Reducing STring

Steve has a string of lowercase characters in range ascii[‘a’..’z’]. He wants to reduce the string to its shortest length by doing a series of operations. In each operation he selects a pair of adjacent lowercase letters that match, and he deletes them. For instance, the string aab could be shortened to b in one operation.

Steve’s task is to delete as many characters as possible using this method and print the resulting string. If the final string is empty, print "Empty String"

## Input Format

A single string, s

## Constraints

 * 1<=s<=100

## Output Format

If the final string is empty, print Empty String; otherwise, print the final non-reducible string.

## Sample Input 0
<pre>
aaabccddd
</pre>
##Sample Output 0
<pre>
abd
</pre>
## Sample Input 1
<pre>
aa
</pre>
## Sample Output 1
<pre>
Empty String
</pre>
