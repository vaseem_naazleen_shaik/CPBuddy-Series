s = input()

def compute(s):
    ans = ""
    s += "*"
    i = 0
    while i < len(s) - 1:
        if s[i] == s[i+1]:
            i += 2
        else:
            ans += s[i]
            i += 1
    return ans

ans = compute(s)

while (ans != compute(ans)):
    ans = compute(ans)

if ans == "":
    print("Empty String")
else:
    print(ans)
