length = int(input())
string = input()
ans = ""
sortedstr = sorted(string)
upper = [i for i in sortedstr if i.isupper()]
lower = [i for i in sortedstr if i.islower()]

i = 0
j = 0
for c in string:
    if c.isupper():
        ans += upper[i]
        i += 1
    else:
        ans += lower[j]
        j += 1

print(ans)
