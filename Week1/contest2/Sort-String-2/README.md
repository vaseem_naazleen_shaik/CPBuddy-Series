# Sort Strings 2

Given a string S consisting of uppercase and lowercase characters. The task is to sort uppercase and lowercase letters separately such that if the ith place in the original string had an Uppercase character then it should not have a lowercase character after being sorted and vice versa.

## Input Format

First line will contain N length of the string. Next line will be the string.

## Constraints

* 1 ≤ N ≤ 10^3

## Output Format

Print the sorted string

## Sample Input 0

<pre>
12
defRTSersUXI
</pre>

## Sample Output 0

<pre>
deeIRSfrsTUX
</pre>

## Explanation 0

Sorted form of given string with the same case of character as that in original string is deeIRSfrsTUX

## Sample Input 1

<pre>
6
srbDKi
</pre>

## Sample Output 1

<pre>
birDKs
</pre>

## Explanation 1

Sorted form of given string with the same case of character will result in output as birDKs.
