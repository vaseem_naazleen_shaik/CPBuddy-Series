ntests = int(input())

def compute():
    string = input()
    setchars = set(string)
    ones = 0
    count = 0
    for c in setchars:
        count = string.count(c)
        if count % 2 == 1:
            if count == 1:
                if ones > 1:
                    return "No"
                else:
                    ones += 1

    if ones < 2:
        return "Yes"

for test in range(ntests):
    print(compute())
        
