# String Palindrome

Arnab is given a string, but being the evil Lord he is, he has his own whims and fantasies about the string he wants to keep with himself. If he can convert a string into a palindrome by rearranging the characters, he will keep that string, otherwise he will discard that string. You’ve to tell if Arnab will keep that string or not.

## Input Format

The first line of input contains the number of test cases T. Each test case contains a single string S.

## Constraints

 * 1 <= Number of test cases, T <= 10

 * 1<=|Length of String S|<=100

## Output Format

For each test case print Yes if Arnab can convert string into a palindrome by rearranging the characters else print No.

## Sample Input 0

<pre>
3
code
pop
abb
</pre>

## Sample Output 0

<pre>
No
Yes
Yes
</pre>
