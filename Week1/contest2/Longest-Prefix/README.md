# Longest prefix

Given a string of character, find the length of longest proper prefix which is also a proper suffix. Example: S = abab lps is 2 because, ab.. is prefix and ..ab is also a suffix.

## Input Format

First line is T number of test cases. 1<=T<=100. Each test case has one line denoting the string of length less than 100000.

## Constraints

* 1<=T<=100

## Output Format

Print length of longest proper prefix which is also a proper suffix.

## Sample Input 0
<pre>
2
abab
aaaa
</pre>

## Sample Output 0
<pre>
2
3
</pre>

## Explanation 0

S = abab lps is 2 because, ab.. is prefix and ..ab is also a suffix.
