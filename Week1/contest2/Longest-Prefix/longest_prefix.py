ntests = int(input())

for test in range(ntests):
    string = input()
    ans = []
    length = len(string)
    i = 0
    while (i < length):
        #print(string[:i], string[-1 * i:],string[:i] == string[-1 * i - 1:])
        if string[:i] == string[-1 * i:]:
            ans.append(string[:i])
        i += 1

    if len(ans) >= 1:
        print(len(ans[-1]))
    else:
        print(0)
            
